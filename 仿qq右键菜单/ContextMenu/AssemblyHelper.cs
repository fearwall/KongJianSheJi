﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Drawing;

namespace ContextMenu
{
    internal class AssemblyHelper
    {
        private static Assembly _assembly = Assembly.GetExecutingAssembly();

        public static Image GetAssemblyImage(string name)
        {
            return Image.FromStream(_assembly.GetManifestResourceStream("ContextMenu.Images." + name));
        }
    }
}
