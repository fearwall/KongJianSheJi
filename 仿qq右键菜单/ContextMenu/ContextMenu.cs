﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace ContextMenu
{
    /// <summary>
    /// 右键菜单
    /// </summary>
    public class ContextMenu : ContextMenuStrip
    {

        #region 构造函数

        public ContextMenu()
            : base()
        {
            this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.DoubleBuffer |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor |
                ControlStyles.UserPaint, true);
            this.Renderer = new QQToolStripRenderer();
        }

        public ContextMenu(IContainer container)
            : base(container)
        {
            this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.DoubleBuffer |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor |
                ControlStyles.UserPaint, true);
            this.Renderer = new QQToolStripRenderer();
        }

        #endregion

    }
}
