﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.contextMenu1 = new ContextMenu.ContextMenu(this.components);
            this.在线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.q我嗓ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.离开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.忙碌ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adfasdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdfToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.请勿打扰ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.隐身ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.离线ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加状态信息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.关闭所有声音ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.锁定QQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.打开主面板ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenu1
            // 
            this.contextMenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.在线ToolStripMenuItem,
            this.q我嗓ToolStripMenuItem,
            this.离开ToolStripMenuItem,
            this.忙碌ToolStripMenuItem,
            this.请勿打扰ToolStripMenuItem,
            this.隐身ToolStripMenuItem,
            this.离线ToolStripMenuItem,
            this.添加状态信息ToolStripMenuItem,
            this.toolStripSeparator1,
            this.关闭所有声音ToolStripMenuItem,
            this.toolStripSeparator3,
            this.锁定QQToolStripMenuItem,
            this.toolStripSeparator4,
            this.打开主面板ToolStripMenuItem,
            this.toolStripSeparator2,
            this.退出ToolStripMenuItem});
            this.contextMenu1.Name = "contextMenu1";
            this.contextMenu1.Size = new System.Drawing.Size(188, 292);
            // 
            // 在线ToolStripMenuItem
            // 
            this.在线ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.在线ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("在线ToolStripMenuItem.Image")));
            this.在线ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.在线ToolStripMenuItem.Name = "在线ToolStripMenuItem";
            this.在线ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.在线ToolStripMenuItem.Text = "在线";
            // 
            // q我嗓ToolStripMenuItem
            // 
            this.q我嗓ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.q我嗓ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("q我嗓ToolStripMenuItem.Image")));
            this.q我嗓ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.q我嗓ToolStripMenuItem.Name = "q我嗓ToolStripMenuItem";
            this.q我嗓ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.q我嗓ToolStripMenuItem.Text = "Q我吧";
            // 
            // 离开ToolStripMenuItem
            // 
            this.离开ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.离开ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("离开ToolStripMenuItem.Image")));
            this.离开ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.离开ToolStripMenuItem.Name = "离开ToolStripMenuItem";
            this.离开ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.离开ToolStripMenuItem.Text = "离开";
            // 
            // 忙碌ToolStripMenuItem
            // 
            this.忙碌ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adfasdfToolStripMenuItem,
            this.asdfToolStripMenuItem,
            this.asdfToolStripMenuItem1});
            this.忙碌ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.忙碌ToolStripMenuItem.Name = "忙碌ToolStripMenuItem";
            this.忙碌ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.忙碌ToolStripMenuItem.Text = "忙碌";
            // 
            // adfasdfToolStripMenuItem
            // 
            this.adfasdfToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.adfasdfToolStripMenuItem.Name = "adfasdfToolStripMenuItem";
            this.adfasdfToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.adfasdfToolStripMenuItem.Text = "adfasdf";
            // 
            // asdfToolStripMenuItem
            // 
            this.asdfToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.asdfToolStripMenuItem.Name = "asdfToolStripMenuItem";
            this.asdfToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.asdfToolStripMenuItem.Text = "asdf";
            // 
            // asdfToolStripMenuItem1
            // 
            this.asdfToolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.asdfToolStripMenuItem1.Name = "asdfToolStripMenuItem1";
            this.asdfToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.asdfToolStripMenuItem1.Text = "asdf";
            // 
            // 请勿打扰ToolStripMenuItem
            // 
            this.请勿打扰ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.请勿打扰ToolStripMenuItem.Name = "请勿打扰ToolStripMenuItem";
            this.请勿打扰ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.请勿打扰ToolStripMenuItem.Text = "请勿打扰";
            // 
            // 隐身ToolStripMenuItem
            // 
            this.隐身ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.隐身ToolStripMenuItem.Name = "隐身ToolStripMenuItem";
            this.隐身ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.隐身ToolStripMenuItem.Text = "隐身";
            // 
            // 离线ToolStripMenuItem
            // 
            this.离线ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.离线ToolStripMenuItem.Name = "离线ToolStripMenuItem";
            this.离线ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.离线ToolStripMenuItem.Text = "离线";
            // 
            // 添加状态信息ToolStripMenuItem
            // 
            this.添加状态信息ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.添加状态信息ToolStripMenuItem.Name = "添加状态信息ToolStripMenuItem";
            this.添加状态信息ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.添加状态信息ToolStripMenuItem.Text = "添加状态信息";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // 关闭所有声音ToolStripMenuItem
            // 
            this.关闭所有声音ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.关闭所有声音ToolStripMenuItem.Name = "关闭所有声音ToolStripMenuItem";
            this.关闭所有声音ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.关闭所有声音ToolStripMenuItem.Text = "关闭所有声音";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(184, 6);
            // 
            // 锁定QQToolStripMenuItem
            // 
            this.锁定QQToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.锁定QQToolStripMenuItem.Name = "锁定QQToolStripMenuItem";
            this.锁定QQToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.L)));
            this.锁定QQToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.锁定QQToolStripMenuItem.Text = "锁定QQ";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(184, 6);
            // 
            // 打开主面板ToolStripMenuItem
            // 
            this.打开主面板ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.打开主面板ToolStripMenuItem.Name = "打开主面板ToolStripMenuItem";
            this.打开主面板ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.打开主面板ToolStripMenuItem.Text = "打开主面板";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(184, 6);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(590, 374);
            this.ContextMenuStrip = this.contextMenu1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.contextMenu1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ContextMenu.ContextMenu contextMenu1;
        private System.Windows.Forms.ToolStripMenuItem 在线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem q我嗓ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 离开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 忙碌ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 请勿打扰ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 隐身ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 离线ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加状态信息ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 关闭所有声音ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 锁定QQToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem 打开主面板ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adfasdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdfToolStripMenuItem1;
    }
}

