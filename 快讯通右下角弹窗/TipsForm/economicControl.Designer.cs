﻿namespace TipsForm
{
    partial class economicControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(economicControl));
            this.stateImages = new System.Windows.Forms.ImageList(this.components);
            this.importImages = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // stateImages
            // 
            this.stateImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("stateImages.ImageStream")));
            this.stateImages.TransparentColor = System.Drawing.Color.Transparent;
            this.stateImages.Images.SetKeyName(0, "bx.png");
            this.stateImages.Images.SetKeyName(1, "cx.png");
            this.stateImages.Images.SetKeyName(2, "dg.png");
            this.stateImages.Images.SetKeyName(3, "els.png");
            this.stateImages.Images.SetKeyName(4, "fg.png");
            this.stateImages.Images.SetKeyName(5, "hg.png");
            this.stateImages.Images.SetKeyName(6, "jnd.png");
            this.stateImages.Images.SetKeyName(7, "mg.png");
            this.stateImages.Images.SetKeyName(8, "nf.png");
            this.stateImages.Images.SetKeyName(9, "adly.png");
            this.stateImages.Images.SetKeyName(10, "other.png");
            this.stateImages.Images.SetKeyName(11, "oyq.png");
            this.stateImages.Images.SetKeyName(12, "rb.png");
            this.stateImages.Images.SetKeyName(13, "rs.png");
            this.stateImages.Images.SetKeyName(14, "tw.png");
            this.stateImages.Images.SetKeyName(15, "xby.png");
            this.stateImages.Images.SetKeyName(16, "xg.png");
            this.stateImages.Images.SetKeyName(17, "xjp.png");
            this.stateImages.Images.SetKeyName(18, "xl.png");
            this.stateImages.Images.SetKeyName(19, "xxl.png");
            this.stateImages.Images.SetKeyName(20, "yd.png");
            this.stateImages.Images.SetKeyName(21, "ydl.png");
            this.stateImages.Images.SetKeyName(22, "ydnxy.png");
            this.stateImages.Images.SetKeyName(23, "yg.png");
            this.stateImages.Images.SetKeyName(24, "zg.png");
            // 
            // importImages
            // 
            this.importImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("importImages.ImageStream")));
            this.importImages.TransparentColor = System.Drawing.Color.Transparent;
            this.importImages.Images.SetKeyName(0, "高");
            this.importImages.Images.SetKeyName(1, "低");
            this.importImages.Images.SetKeyName(2, "中");
            // 
            // economicControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "economicControl";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList stateImages;
        private System.Windows.Forms.ImageList importImages;
    }
}
