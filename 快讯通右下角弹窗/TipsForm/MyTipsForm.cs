﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace KXTPC
{
    public partial class MyTipsForm : Form
    {
        private bool isMouseDown = false;
        private Point mouseOffset;
        private Point FormLocation;
        int time = 0;
        object LockObject = new object();
        bool isNews;
        
        public MyTipsForm()
        {
            InitializeComponent();
        }

        private void OnShow()
        {
            if (!tipsTimer.Enabled)
            {
                tipsTimer.Enabled = true;
            }
            time = 0;

            if (this.Left > -3000 || this.Top > -3000) return;
            Size screenSize = Screen.GetWorkingArea(this).Size;
            this.Location = new Point(screenSize.Width - this.Width - 6, screenSize.Height - this.Height - 6);
        }

        /// <summary>
        /// 显示快讯数据
        /// </summary>
        /// <param name="listMsg"></param>
        public void ShowMsg()
        {
            this.BeginInvoke(new Action(() =>
            {
                this.OnShow();
            }));
        }

        private void detail_Click(object sender, EventArgs e)
        {
        }

        private void closeBox_Click(object sender, EventArgs e)
        {
            this.Location = new Point(-3000, -3000);
            Close();
        }

        #region 窗体移动
        private void tipsTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                FormLocation = this.Location;
                mouseOffset = MousePosition;
            }
        }

        private void tipsTitle_MouseMove(object sender, MouseEventArgs e)
        {
            int x = 0;
            int y = 0;
            if (isMouseDown)
            {
                Point pt = Control.MousePosition;
                x = mouseOffset.X - pt.X;
                y = mouseOffset.Y - pt.Y;
                Location = new Point(FormLocation.X - x, FormLocation.Y - y);
            }
        }

        private void tipsTitle_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }
        #endregion

        private void tipsTimer_Tick(object sender, EventArgs e)
        {
            time++;
            if (time > 15)
            {
                BeginInvoke(new Action(()=> {
                    tipsTimer.Enabled = false;
                    this.Location = new Point(-3000, -3000);

                }));                
            }
        }

        private void TipsForm_MouseEnter(object sender, EventArgs e)
        {
            tipsTimer.Enabled = false;
        }

        private void TipsForm_MouseLeave(object sender, EventArgs e)
        {
            tipsTimer.Enabled = true;
        }
    }
}
