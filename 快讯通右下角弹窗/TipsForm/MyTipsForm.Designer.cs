﻿namespace KXTPC
{
    partial class MyTipsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyTipsForm));
            this.tipsTitle = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.detail = new System.Windows.Forms.Label();
            this.tipsTimer = new System.Windows.Forms.Timer(this.components);
            this.bannerList = new System.Windows.Forms.ImageList(this.components);
            this.economicControl1 = new TipsForm.economicControl();
            this.tipsTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tipsTitle
            // 
            this.tipsTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(239)))), ((int)(((byte)(248)))));
            this.tipsTitle.Controls.Add(this.pictureBox2);
            this.tipsTitle.Controls.Add(this.pictureBox1);
            this.tipsTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.tipsTitle.Location = new System.Drawing.Point(0, 0);
            this.tipsTitle.Margin = new System.Windows.Forms.Padding(0);
            this.tipsTitle.Name = "tipsTitle";
            this.tipsTitle.Size = new System.Drawing.Size(323, 30);
            this.tipsTitle.TabIndex = 1;
            this.tipsTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tipsTitle_MouseDown);
            this.tipsTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tipsTitle_MouseMove);
            this.tipsTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tipsTitle_MouseUp);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(295, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.closeBox_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 21);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // detail
            // 
            this.detail.AutoSize = true;
            this.detail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.detail.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.detail.Location = new System.Drawing.Point(0, 204);
            this.detail.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.detail.Name = "detail";
            this.detail.Padding = new System.Windows.Forms.Padding(0, 3, 0, 4);
            this.detail.Size = new System.Drawing.Size(44, 26);
            this.detail.TabIndex = 2;
            this.detail.Text = "详细...";
            this.detail.Click += new System.EventHandler(this.detail_Click);
            // 
            // tipsTimer
            // 
            this.tipsTimer.Interval = 1000;
            this.tipsTimer.Tick += new System.EventHandler(this.tipsTimer_Tick);
            // 
            // bannerList
            // 
            this.bannerList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("bannerList.ImageStream")));
            this.bannerList.TransparentColor = System.Drawing.Color.Transparent;
            this.bannerList.Images.SetKeyName(0, "澳大利亚");
            this.bannerList.Images.SetKeyName(1, "巴西");
            this.bannerList.Images.SetKeyName(2, "其他");
            // 
            // economicControl1
            // 
            this.economicControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.economicControl1.Location = new System.Drawing.Point(0, 30);
            this.economicControl1.Name = "economicControl1";
            this.economicControl1.Size = new System.Drawing.Size(323, 174);
            this.economicControl1.TabIndex = 3;
            // 
            // MyTipsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(323, 230);
            this.Controls.Add(this.economicControl1);
            this.Controls.Add(this.detail);
            this.Controls.Add(this.tipsTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MyTipsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TipsForm";
            this.TopMost = true;
            this.MouseEnter += new System.EventHandler(this.TipsForm_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.TipsForm_MouseLeave);
            this.tipsTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel tipsTitle;
        private System.Windows.Forms.Label detail;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Timer tipsTimer;
        private System.Windows.Forms.ImageList bannerList;
        private TipsForm.economicControl economicControl1;
    }
}