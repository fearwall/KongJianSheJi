﻿using System;

namespace TipsForm
{
    /// <summary>
    /// 示例用，如果项目里已有快讯和财经的类，用项目里的代替
    /// </summary>
    class EconomicNews
    {
        public string Title { get; set; }
        public DateTime Time { get; set; }
        public string Importance { get; set; }
    }
    class EconomicCalendar
    {

        public string Title { get; set; }
        public DateTime Time { get; set; }
        public string Importance { get; set; }
        public string Before { get; set; }
        public string Forecast { get; set; }
        public string Reality { get; set; }
        public string Effect { get; set; }
        public string State { get; set; }
    }
}