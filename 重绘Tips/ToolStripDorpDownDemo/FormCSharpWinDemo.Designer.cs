﻿namespace CSharpWinDemo
{
    partial class FormCSharpWinDemo
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBottom = new System.Windows.Forms.Panel();
            this.buttonAbout = new System.Windows.Forms.Button();
            this.linkLabelCSharpWin = new System.Windows.Forms.LinkLabel();
            this.panelTop = new System.Windows.Forms.Panel();
            this.buttonResizeControl = new System.Windows.Forms.Button();
            this.buttonRegionControl = new System.Windows.Forms.Button();
            this.buttonNormalControl = new System.Windows.Forms.Button();
            this.panelBottom.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.buttonAbout);
            this.panelBottom.Controls.Add(this.linkLabelCSharpWin);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 157);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(416, 32);
            this.panelBottom.TabIndex = 0;
            // 
            // buttonAbout
            // 
            this.buttonAbout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAbout.FlatAppearance.BorderSize = 0;
            this.buttonAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAbout.Location = new System.Drawing.Point(338, 6);
            this.buttonAbout.Name = "buttonAbout";
            this.buttonAbout.Size = new System.Drawing.Size(75, 23);
            this.buttonAbout.TabIndex = 16;
            this.buttonAbout.Text = "关于...";
            this.buttonAbout.UseVisualStyleBackColor = true;
            // 
            // linkLabelCSharpWin
            // 
            this.linkLabelCSharpWin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelCSharpWin.AutoSize = true;
            this.linkLabelCSharpWin.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabelCSharpWin.Location = new System.Drawing.Point(74, 9);
            this.linkLabelCSharpWin.Name = "linkLabelCSharpWin";
            this.linkLabelCSharpWin.Size = new System.Drawing.Size(258, 14);
            this.linkLabelCSharpWin.TabIndex = 15;
            this.linkLabelCSharpWin.TabStop = true;
            this.linkLabelCSharpWin.Text = "www.csharpwin.com(CS 程序员之窗)";
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.buttonResizeControl);
            this.panelTop.Controls.Add(this.buttonRegionControl);
            this.panelTop.Controls.Add(this.buttonNormalControl);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(416, 157);
            this.panelTop.TabIndex = 1;
            // 
            // buttonResizeControl
            // 
            this.buttonResizeControl.Location = new System.Drawing.Point(252, 66);
            this.buttonResizeControl.Name = "buttonResizeControl";
            this.buttonResizeControl.Size = new System.Drawing.Size(75, 23);
            this.buttonResizeControl.TabIndex = 2;
            this.buttonResizeControl.Text = "改变大小";
            this.buttonResizeControl.UseVisualStyleBackColor = true;
            // 
            // buttonRegionControl
            // 
            this.buttonRegionControl.Location = new System.Drawing.Point(156, 66);
            this.buttonRegionControl.Name = "buttonRegionControl";
            this.buttonRegionControl.Size = new System.Drawing.Size(75, 23);
            this.buttonRegionControl.TabIndex = 1;
            this.buttonRegionControl.Text = "异性窗体";
            this.buttonRegionControl.UseVisualStyleBackColor = true;
            // 
            // buttonNormalControl
            // 
            this.buttonNormalControl.Location = new System.Drawing.Point(65, 66);
            this.buttonNormalControl.Name = "buttonNormalControl";
            this.buttonNormalControl.Size = new System.Drawing.Size(75, 23);
            this.buttonNormalControl.TabIndex = 0;
            this.buttonNormalControl.Text = "正常窗体";
            this.buttonNormalControl.UseVisualStyleBackColor = true;
            // 
            // FormCSharpWinDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 189);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelBottom);
            this.Name = "FormCSharpWinDemo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PopupControlHost 例子";
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.panelTop.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.LinkLabel linkLabelCSharpWin;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.Button buttonResizeControl;
        private System.Windows.Forms.Button buttonRegionControl;
        private System.Windows.Forms.Button buttonNormalControl;
    }
}

