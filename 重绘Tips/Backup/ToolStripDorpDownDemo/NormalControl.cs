﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CSharpWin;

namespace ToolStripDorpDownDemo
{
    public partial class NormalControl : UserControl
    {
        PopupControlHost _host;
        RegionControl control;

        public NormalControl()
        {
            InitializeComponent();
            control = new RegionControl();
            _host = new PopupControlHost(control);
            _host.ChangeRegion = true;
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            _host.Show(buttonTest,true);
        }
    }
}
