﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using CSharpWin;
using ToolStripDorpDownDemo;

namespace CSharpWinDemo
{
    public partial class FormCSharpWinDemo : Form
    {
        private SystemMenuNativeWindow _systemMenuNativeWindow;

        private NormalControl _normalControl;
        private NormalControl _resizeControl;
        private RegionControl _regionControl;

        private PopupControlHost _normoalHost;
        private PopupControlHost _regionHost;
        private PopupControlHost _resizeHost;

        public FormCSharpWinDemo()
        {
            InitializeComponent();
            InitEvents();

            _normalControl = new NormalControl();
            _resizeControl = new NormalControl();
            _regionControl = new RegionControl();

            _normoalHost = new PopupControlHost(_normalControl);
            _regionHost = new PopupControlHost(_regionControl);
            _resizeHost = new PopupControlHost(_resizeControl);

            _regionHost.ChangeRegion = true;//设置显示区域。
            _regionHost.Opacity = 0.8F;//设置透明度。
            _resizeHost.CanResize = true;//设置可以改变大小。
            _resizeHost.OpenFocused = true;//把焦点设置到弹出窗体上。
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = string.Format(
                    "CS 程序员之窗 - {0}", value);
            }
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (_systemMenuNativeWindow == null)
            {
                _systemMenuNativeWindow = new SystemMenuNativeWindow(this);
            }

            _systemMenuNativeWindow.AppendSeparator();
            _systemMenuNativeWindow.AppendMenu(
                1001,
                "访问 www.csharpwin.com",
                delegate(object sender, EventArgs e)
                {
                    Process.Start("www.csharpwin.com");
                });

            _systemMenuNativeWindow.AppendMenu(
                1000,
                "关于...(&A)",
                delegate(object sender, EventArgs e)
                {
                    AboutBoxCSharpWinDemo about = new AboutBoxCSharpWinDemo();
                    about.ShowDialog();
                });
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);
            if (_systemMenuNativeWindow != null)
            {
                _systemMenuNativeWindow.Dispose();
                _systemMenuNativeWindow = null;
            }
        }

        private void InitEvents()
        {
            linkLabelCSharpWin.Click += delegate(object sender, EventArgs e)
            {
                Process.Start("www.csharpwin.com");
            };

            buttonAbout.Click += delegate(object sender, EventArgs e)
            {
                AboutBoxCSharpWinDemo about = new AboutBoxCSharpWinDemo();
                about.ShowDialog();
            };

            buttonNormalControl.Click += delegate(object sender, EventArgs e)
            {
                _normoalHost.Show(buttonNormalControl, true);
            };

            buttonRegionControl.Click += delegate(object sender, EventArgs e)
            {
                _regionHost.Show(buttonRegionControl, true);
            };

            buttonResizeControl.Click += delegate(object sender, EventArgs e)
           {
               _resizeHost.Show(buttonResizeControl);
           };
        }

        private void ButtonAboutClick(object sender, EventArgs e)
        {
            AboutBoxCSharpWinDemo f = new AboutBoxCSharpWinDemo();
            f.ShowDialog();
        }
    }
}