﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace 控件重绘
{
    public partial class RadioBtn : UserControl
    {
        protected bool IsEnter = false;
        private bool checke = false;
        public bool Checked
        {
            get{return checke;}
            set { checke = value; }
        }
        public RadioBtn()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.EnableNotifyMessage, true);
        }
        private Image radioImage = 控件重绘.Properties.Resources.radio;
        public Image RadioImage
        {
            get{return radioImage;}
            set{radioImage = value;}
        }
        private Image radioImage2 = 控件重绘.Properties.Resources.radioSelect;
        public Image RadioImage2
        {
            get { return radioImage2; }
            set { radioImage2 = value; }
        }

        private string texts = "radioBtn";
        [DefaultValue("radioBtn")]
        public string Texts
        {
            get { return texts; }
            set { texts = value; }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Image image = RadioImage;
            if (IsEnter) image = RadioImage2;
            if (Checked) image = RadioImage2;

            e.Graphics.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            e.Graphics.DrawString(Texts, Font, new SolidBrush(ForeColor), new Point(24, 2));
            base.OnPaint(e);
        }

        private void RadioBtns_MouseEnter(object sender, EventArgs e)
        {
            IsEnter = true;
            Invalidate();
        }

        private void RadioBtns_MouseLeave(object sender, EventArgs e)
        {
            IsEnter = false;
            Invalidate();
        }

        private void RadioBtns_Click(object sender, EventArgs e)
        {
            foreach (Control con in this.Parent.Controls)
            {
                if (con.GetType() == typeof(RadioBtn))
                {
                    ((RadioBtn)con).Checked = false;
                }
            }
            this.Checked = true;
            this.Parent.Invalidate(true);
        }
    }
}
