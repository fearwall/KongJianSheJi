﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace 控件重绘
{
    public partial class PaintControl : Form
    {
        Timer timer = new Timer();
        public PaintControl()
        {
            InitializeComponent();
            timer.Tick += timer_Tick;
            timer.Interval = 10;
            timer.Start();
            processBar1.Maximum = 300;
            label1.Text = "label";
        }

        void timer_Tick(object sender, EventArgs e)
        {
            processBar1.Value++;
            if (processBar1.Value >= 300)
                processBar1.Value = 0;
        }

        private void myButtonCheck1_Click(object sender, EventArgs e)
        {

        }
    }
}
