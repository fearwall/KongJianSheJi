﻿namespace 控件重绘
{
    partial class RadioBtn
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // RadioBtn
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.Name = "RadioBtn";
            this.Size = new System.Drawing.Size(101, 34);
            this.Click += new System.EventHandler(this.RadioBtns_Click);
            this.MouseEnter += new System.EventHandler(this.RadioBtns_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.RadioBtns_MouseLeave);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
