﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace 控件重绘
{
    public partial class Labels : UserControl
    {
        private string text = "Label";
        [CategoryAttribute("外观"), Browsable(true), DescriptionAttribute("Text属性")]//DisplayName("自定义属性"), 
        public string Txt
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
        public Labels()
        {
            InitializeComponent();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Size size = TextRenderer.MeasureText(Txt, this.Font);
            e.Graphics.DrawString(Txt, Font, new SolidBrush(ForeColor), new Point((Width - size.Width) / 2, (Height - size.Height) / 2));
            base.OnPaint(e);
        }
    }
}
