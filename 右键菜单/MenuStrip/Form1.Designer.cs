﻿namespace MenuStrip
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Drawing.StringFormat stringFormat6 = new System.Drawing.StringFormat();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.menu = new Zbc.UserControl.CustomMenuStrip();
            this.textMenuItem1 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem4 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem5 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem6 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem7 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem8 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem9 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem10 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem11 = new Zbc.UserControl.TextMenuItem();
            this.textMenuItem2 = new Zbc.UserControl.TextMenuItem();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // menu
            // 
            this.menu.ArrowColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.menu.ArrowHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(132)))), ((int)(((byte)(244)))));
            this.menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.menu.BoderColor = System.Drawing.Color.Gainsboro;
            this.menu.DisableFontColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.menu.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.menu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            stringFormat6.Alignment = System.Drawing.StringAlignment.Near;
            stringFormat6.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.None;
            stringFormat6.LineAlignment = System.Drawing.StringAlignment.Center;
            stringFormat6.Trimming = System.Drawing.StringTrimming.Character;
            this.menu.Format = stringFormat6;
            this.menu.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(200)))), ((int)(((byte)(249)))));
            this.menu.ImageSize = new System.Drawing.Size(16, 16);
            this.menu.ImageWidth = 32;
            this.menu.Items.Add(this.textMenuItem1);
            this.menu.Items.Add(this.textMenuItem2);
            this.menu.MarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.menu.Name = null;
            this.menu.QuickKeyWidthSp = 15;
            this.menu.SelectBackColor = System.Drawing.Color.Empty;
            this.menu.SelectColor = System.Drawing.Color.Empty;
            this.menu.ShadowSize = 7;
            this.menu.ShowBorder = false;
            this.menu.Tag = null;
            this.menu.Text = null;
            this.menu.TextHeightSp = 10;
            this.menu.TextWidthSp = 20;
            this.menu.ItemClicked += new System.Action<Zbc.UserControl.TextMenuItem>(this.menu_ItemClicked);
            // 
            // textMenuItem1
            // 
            this.textMenuItem1.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem1.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem1.Checked = true;
            this.textMenuItem1.Font = null;
            this.textMenuItem1.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem1.Image = null;
            this.textMenuItem1.Items.Add(this.textMenuItem4);
            this.textMenuItem1.Items.Add(this.textMenuItem5);
            this.textMenuItem1.Name = "textMenuItem1";
            this.textMenuItem1.QuickKey = null;
            this.textMenuItem1.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem1.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem1.ShadowSize = 0;
            this.textMenuItem1.ShowBorder = false;
            this.textMenuItem1.ShowShadow = false;
            this.textMenuItem1.Tag = null;
            this.textMenuItem1.Text = "textMenuItem1";
            // 
            // textMenuItem4
            // 
            this.textMenuItem4.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem4.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem4.Font = null;
            this.textMenuItem4.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem4.Image = null;
            this.textMenuItem4.Name = "textMenuItem4";
            this.textMenuItem4.QuickKey = null;
            this.textMenuItem4.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem4.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem4.ShadowSize = 0;
            this.textMenuItem4.ShowBorder = false;
            this.textMenuItem4.ShowShadow = false;
            this.textMenuItem4.Tag = null;
            this.textMenuItem4.Text = "textMenuItem4";
            // 
            // textMenuItem5
            // 
            this.textMenuItem5.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem5.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem5.Font = null;
            this.textMenuItem5.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem5.Image = null;
            this.textMenuItem5.Items.Add(this.textMenuItem6);
            this.textMenuItem5.Items.Add(this.textMenuItem7);
            this.textMenuItem5.Items.Add(this.textMenuItem8);
            this.textMenuItem5.Name = "textMenuItem5";
            this.textMenuItem5.QuickKey = null;
            this.textMenuItem5.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem5.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem5.ShadowSize = 0;
            this.textMenuItem5.ShowBorder = false;
            this.textMenuItem5.ShowShadow = false;
            this.textMenuItem5.Tag = null;
            this.textMenuItem5.Text = "textMenuItem5";
            // 
            // textMenuItem6
            // 
            this.textMenuItem6.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem6.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem6.Font = null;
            this.textMenuItem6.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem6.Image = null;
            this.textMenuItem6.Name = "textMenuItem6";
            this.textMenuItem6.QuickKey = null;
            this.textMenuItem6.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem6.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem6.ShadowSize = 0;
            this.textMenuItem6.ShowBorder = false;
            this.textMenuItem6.ShowShadow = false;
            this.textMenuItem6.Tag = null;
            this.textMenuItem6.Text = "textMenuItem6";
            // 
            // textMenuItem7
            // 
            this.textMenuItem7.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem7.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem7.Font = null;
            this.textMenuItem7.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem7.Image = null;
            this.textMenuItem7.Name = "textMenuItem7";
            this.textMenuItem7.QuickKey = null;
            this.textMenuItem7.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem7.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem7.ShadowSize = 0;
            this.textMenuItem7.ShowBorder = false;
            this.textMenuItem7.ShowShadow = false;
            this.textMenuItem7.Tag = null;
            this.textMenuItem7.Text = "textMenuItem7";
            // 
            // textMenuItem8
            // 
            this.textMenuItem8.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem8.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem8.Font = null;
            this.textMenuItem8.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem8.Image = null;
            this.textMenuItem8.Items.Add(this.textMenuItem9);
            this.textMenuItem8.Items.Add(this.textMenuItem10);
            this.textMenuItem8.Items.Add(this.textMenuItem11);
            this.textMenuItem8.Name = "textMenuItem8";
            this.textMenuItem8.QuickKey = null;
            this.textMenuItem8.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem8.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem8.ShadowSize = 0;
            this.textMenuItem8.ShowBorder = false;
            this.textMenuItem8.ShowShadow = false;
            this.textMenuItem8.Tag = null;
            this.textMenuItem8.Text = "textMenuItem8";
            // 
            // textMenuItem9
            // 
            this.textMenuItem9.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem9.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem9.Font = null;
            this.textMenuItem9.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem9.Image = null;
            this.textMenuItem9.Name = "textMenuItem9";
            this.textMenuItem9.QuickKey = null;
            this.textMenuItem9.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem9.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem9.ShadowSize = 0;
            this.textMenuItem9.ShowBorder = false;
            this.textMenuItem9.ShowShadow = false;
            this.textMenuItem9.Tag = null;
            this.textMenuItem9.Text = "textMenuItem9";
            // 
            // textMenuItem10
            // 
            this.textMenuItem10.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem10.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem10.Font = null;
            this.textMenuItem10.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem10.Image = null;
            this.textMenuItem10.Name = "textMenuItem10";
            this.textMenuItem10.QuickKey = null;
            this.textMenuItem10.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem10.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem10.ShadowSize = 0;
            this.textMenuItem10.ShowBorder = false;
            this.textMenuItem10.ShowShadow = false;
            this.textMenuItem10.Tag = null;
            this.textMenuItem10.Text = "textMenuItem10";
            // 
            // textMenuItem11
            // 
            this.textMenuItem11.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem11.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem11.Font = null;
            this.textMenuItem11.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem11.Image = null;
            this.textMenuItem11.Name = "textMenuItem11";
            this.textMenuItem11.QuickKey = null;
            this.textMenuItem11.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem11.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem11.ShadowSize = 0;
            this.textMenuItem11.ShowBorder = false;
            this.textMenuItem11.ShowShadow = false;
            this.textMenuItem11.Tag = null;
            this.textMenuItem11.Text = "textMenuItem11";
            // 
            // textMenuItem2
            // 
            this.textMenuItem2.BoderColor = System.Drawing.Color.Empty;
            this.textMenuItem2.Bounds = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.textMenuItem2.Font = null;
            this.textMenuItem2.ForeColor = System.Drawing.Color.Empty;
            this.textMenuItem2.Image = null;
            this.textMenuItem2.Name = "textMenuItem2";
            this.textMenuItem2.QuickKey = null;
            this.textMenuItem2.SelectBackColor = System.Drawing.Color.Empty;
            this.textMenuItem2.SelectColor = System.Drawing.Color.Empty;
            this.textMenuItem2.ShadowSize = 0;
            this.textMenuItem2.ShowBorder = false;
            this.textMenuItem2.ShowShadow = false;
            this.textMenuItem2.Tag = null;
            this.textMenuItem2.Text = "textMenuItem2";
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(336, 289);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.Enter += new System.EventHandler(this.Form1_Enter);
            this.Leave += new System.EventHandler(this.Form1_Leave);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.Form1_Validating);
            this.Validated += new System.EventHandler(this.Form1_Validated);
            this.ResumeLayout(false);

        }

        #endregion

        private Zbc.UserControl.CustomMenuStrip menu;
        private Zbc.UserControl.TextMenuItem textMenuItem1;
        private Zbc.UserControl.TextMenuItem textMenuItem2;
        private Zbc.UserControl.TextMenuItem textMenuItem4;
        private Zbc.UserControl.TextMenuItem textMenuItem5;
        private Zbc.UserControl.TextMenuItem textMenuItem6;
        private Zbc.UserControl.TextMenuItem textMenuItem7;
        private Zbc.UserControl.TextMenuItem textMenuItem8;
        private Zbc.UserControl.TextMenuItem textMenuItem9;
        private Zbc.UserControl.TextMenuItem textMenuItem10;
        private Zbc.UserControl.TextMenuItem textMenuItem11;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

