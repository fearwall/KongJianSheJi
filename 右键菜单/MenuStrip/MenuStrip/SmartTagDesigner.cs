﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Security.Permissions;
using System.Text;
using Zbc.UserControl;

namespace Zbc.UserControl
{
    /// <summary>
    /// 自定义智能标记
    /// </summary>
    [PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class SmartTagDesigner : ComponentDesigner
    {
        private DesignerActionListCollection actionLists;
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (null == actionLists)
                {
                    actionLists = new DesignerActionListCollection();
                    actionLists.Add(new MenuTagActionList(this.Component));
                }
                return actionLists;
            }
        }

        public SmartTagDesigner()
        {
            DesignerVerb verb2 = new DesignerVerb("Click", new EventHandler(OnClick));
            //this.Verbs.Add(verb2);
        }
        public void OnClick(object sender, EventArgs e)
        {
            //MessageBox.Show("Click");
        }

        // 1、可以设计Component的默认事件创建方法签名，并将用户的光标定位到该位置。
        // 2、也可以为Component添加双击时要进行的操作。
        public override void DoDefaultAction()
        {
            //MessageBox.Show(" You double clicked.");
        }

        // 从工具箱上拖放一个Component时，执行此方法。
        public override void InitializeNewComponent(System.Collections.IDictionary defaultValues)
        {
            /*
             在创建一个新的Component时，把Parent Component的名称写到ParentName属性中去。
             */
            // 因为是在Design Time，所以ParentComponent的Site是存在的。
            string parentName = ((Component)this.ParentComponent).Site.Name;

            //((Customer)(this.Component)).ParentComponentName = parentName;

            ///*
            // 对Component设置默认属性。
            // */
            //((Customer)(this.Component)).Age = 18;
        }


        // 可以对Component的属性进行操作，如移出、添加。
        protected override void PostFilterProperties(System.Collections.IDictionary properties)
        {
            // 提供有关组件属性 (Attribute) 的信息，如组件的属性 (Attribute)、属性 (Property) 和事件。
            // 删除Address属性。
            properties.Remove("Address");
        }
        // PostFilterAttributes、PostFilterEvents、PreFilterAttributes、PreFilterEvents、PreFilterProperties请大家自己研究。

        /*
         对Component设置默认属性（过时，保持向下兼容），现在改为InitializeNewComponent中进行初始化。
        */
        //public override void OnSetComponentDefaults()
        //{
        //    base.OnSetComponentDefaults();
        //    //((Customer)(this.Component)).Age = 18;
        //}

    }


    // DesignerActionList-derived class defines smart tag entries and
    // resultant actions.
    public class MenuTagActionList : DesignerActionList
    {
        private CustomMenuStrip editorControl;
        private DesignerActionUIService designerActionUISvc = null;

        public MenuTagActionList(IComponent component)
            : base(component)
        {
            this.editorControl = component as CustomMenuStrip;
            //用于刷新智能标记面板
            this.designerActionUISvc = GetService(typeof(DesignerActionUIService)) as DesignerActionUIService;
        }

        // Helper method to retrieve control properties. Use of 
        // GetProperties enables undo and menu updates to work properly.
        private PropertyDescriptor GetPropertyByName(String propName)
        {
            PropertyDescriptor prop;
            prop = TypeDescriptor.GetProperties(editorControl)[propName];
            if (null == prop)
                throw new ArgumentException("Matching ColorLabel property not found!", propName);
            else
                return prop;
        }

        // Properties that are targets of DesignerActionPropertyItem entries.
        public bool ShowBorder
        {
            get
            {
                return editorControl.ShowBorder;
            }
            set
            {
                GetPropertyByName("ShowBorder").SetValue(editorControl, value);
            }
        }

        public Color BoderColor
        {
            get
            {
                return editorControl.BoderColor;
            }
            set
            {
                GetPropertyByName("BoderColor").SetValue(editorControl, value);
            }
        }
        public Color BackColor
        {
            get
            {
                return editorControl.BackColor;
            }
            set
            {
                GetPropertyByName("BackColor").SetValue(editorControl, value);
            }
        }

        public Font Font
        {
            get
            {
                return editorControl.Font;
            }
            set
            {
                GetPropertyByName("Font").SetValue(editorControl, value);
            }
        }

        //取反色
        public void InvertColors()
        {
            //this.editorControl.Items.
            Color currentBackColor = editorControl.BackColor;
            BackColor = Color.FromArgb(
                255 - currentBackColor.R,
                255 - currentBackColor.G,
                255 - currentBackColor.B);

            Color currentForeColor = editorControl.BoderColor;
            BoderColor = Color.FromArgb(
                255 - currentForeColor.R,
                255 - currentForeColor.G,
                255 - currentForeColor.B);
            this.designerActionUISvc.Refresh(this.Component);
        }

        // 在智能标签上显示的属性
        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection items = new DesignerActionItemCollection();
            //Define static section header entries.
            //childItems.Add(new DesignerActionHeaderItem("Appearance"));
            //childItems.Add(new DesignerActionHeaderItem("Information"));

            //if (!LockColors)
            {
                items.Add(new DesignerActionPropertyItem("BackColor",
                                 "Back Color", "Appearance",
                                 "指定控件背景颜色"));
                items.Add(new DesignerActionPropertyItem("BoderColor",
                                 "Boder Color", "Appearance",
                                 "Selects the foreground color."));
                items.Add(new DesignerActionPropertyItem("Font",
                                 "Font", "Appearance",
                                 "Sets the display text."));
                //This next method item is also added to the context menu 
                // (as a designer verb).
                items.Add(new DesignerActionMethodItem(this,
                                 "InvertColors", "编辑项...",
                                 "Appearance",
                                 "Inverts the fore and background colors.",
                                  true));
            }
            return items;
        }
    }
}
