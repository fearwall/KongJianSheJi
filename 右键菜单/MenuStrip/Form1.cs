﻿using MenuStrip.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Zbc.UserControl;

namespace MenuStrip
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitMenu();
            notifyIcon1.Visible = true;
            notifyIcon1.MouseUp += NotifyIcon1_MouseUp;
        }

        private void NotifyIcon1_MouseUp(object sender, MouseEventArgs e)
        {
            menu.Show(this);
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            menu.Show(this);
        }

        private void menu_ItemClicked(TextMenuItem obj)
        {
            MessageBox.Show(obj.Text);
        }
        private void InitMenu()
        {
            menu.BackColor = Color.FromArgb(200,200,200,200);
            menu.TextWidthSp = 0;
            for (int i = 1; i < 9; i++)
            {
                var it = new TextMenuItem(i.ToString()) { Image = Resources.man, QuickKey = "Ctrl+" + i.ToString() };
                menu.Items.Add(it);
            }

            for (int k = 0; k < 3; k++)
            {
                menu.Items.Add(new SplitMenuItem());
                var dropItem = new TextMenuItem("多级菜单") { Image = Resources.man };
                var childMenu1 = new TextMenuItem("子菜单1") { Image = Resources.man, QuickKey = "Ctrl+X" };
                var childMenu2 = new TextMenuItem("子菜单2") { Image = Resources.man };
                var childMenu3 = new TextMenuItem("无效菜单") { Image = Resources.man, Enabled = false };
                for (int i = 0; i < 10; i++)
                {
                    var 三级菜单 = new TextMenuItem("长长长长长长菜单" + (i + 1).ToString()) { Image = Resources.man };
                    if (i % 3 == 1)
                    {
                        childMenu2.Items.Add(new SplitMenuItem());
                    }
                    childMenu2.Items.Add(三级菜单);
                }
                dropItem.Items.Add(childMenu1);
                dropItem.Items.Add(new SplitMenuItem());
                dropItem.Items.Add(childMenu2);
                dropItem.Items.Add(childMenu3);
                menu.Items.Add(dropItem);
            }
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {

        }

        private void Form1_Leave(object sender, EventArgs e)
        {

        }

        private void Form1_Validated(object sender, EventArgs e)
        {

        }

        private void Form1_Validating(object sender, CancelEventArgs e)
        {

        }

        private void Form1_Activated(object sender, EventArgs e)
        {

        }

        private void Form1_Enter(object sender, EventArgs e)
        {

        }
    }
}
