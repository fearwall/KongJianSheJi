﻿namespace Demo
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            设计时支持.CommandItem commandItem1 = new 设计时支持.CommandItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.colorLabel1 = new SmartTags.ColorLabel();
            this.itemCollectionEditor = new 设计时支持.ItemCollectionEditor();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // colorLabel1
            // 
            this.colorLabel1.AutoSize = true;
            this.colorLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.colorLabel1.ColorLocked = false;
            this.colorLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colorLabel1.Location = new System.Drawing.Point(145, 88);
            this.colorLabel1.Name = "colorLabel1";
            this.colorLabel1.Size = new System.Drawing.Size(71, 12);
            this.colorLabel1.TabIndex = 4;
            this.colorLabel1.Text = "colorLabel1";
            // 
            // itemCollectionEditor
            // 
            this.itemCollectionEditor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.itemCollectionEditor.ColorLocked = false;
            this.itemCollectionEditor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.itemCollectionEditor.Grade = "一年级";
            this.itemCollectionEditor.Sex = 设计时支持.Sex.女;
            this.itemCollectionEditor.Text = "22";
            commandItem1.CommandActionType = 设计时支持.CommandActionType.Save;
            commandItem1.EnableViewState = true;
            commandItem1.LightShape = 设计时支持.MarqueeLightShape.Square;
            this.itemCollectionEditor.ToolBarItems.Add(commandItem1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.colorLabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private SmartTags.ColorLabel colorLabel1;
        private 设计时支持.ItemCollectionEditor itemCollectionEditor;
    }
}

