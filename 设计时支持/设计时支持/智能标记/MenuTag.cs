﻿// Pull model smart tag example.
// Need references to System.dll, System.Windows.Forms.dll, 
// System.Design.dll, and System.Drawing.dll.

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Text;
using System.Reflection;
using System.Windows.Forms.Design;
using System.Security.Permissions;
using 设计时支持;

namespace SmartTags
{
    // Designer for the ColorLabel control with support for a smart 
    // tag panel.
    // Must add reference to System.Design.dll
    [PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class MenuTagDesigner : ComponentDesigner
    {
        private DesignerActionListCollection actionLists;
        // Use pull model to populate smart tag menu.
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if (null == actionLists)
                {
                    actionLists = new DesignerActionListCollection();
                    actionLists.Add(new MenuTagActionList(this.Component));
                }
                return actionLists;
            }
        }

        public MenuTagDesigner()
        {
            DesignerVerb verb2 = new DesignerVerb("Click", new EventHandler(OnClick));
            this.Verbs.Add(verb2);
        }
        public void OnClick(object sender, EventArgs e)
        {
            MessageBox.Show("Click");
        }

        // 1、可以设计Component的默认事件创建方法签名，并将用户的光标定位到该位置。
        // 2、也可以为Component添加双击时要进行的操作。
        public override void DoDefaultAction()
        {
            MessageBox.Show(" You double clicked.");
        }

        // 从工具箱上拖放一个Component时，执行此方法。
        public override void InitializeNewComponent(System.Collections.IDictionary defaultValues)
        {
            /*
             在创建一个新的Component时，把Parent Component的名称写到ParentName属性中去。
             */
            // 因为是在Design Time，所以ParentComponent的Site是存在的。
            string parentName = ((Component)this.ParentComponent).Site.Name;

            //((Customer)(this.Component)).ParentComponentName = parentName;

            ///*
            // 对Component设置默认属性。
            // */
            //((Customer)(this.Component)).Age = 18;
        }
        // InitializeExistingComponent、InitializeNonDefault请大家自己研究。


        // 可以对Component的属性进行操作，如移出、添加。
        protected override void PostFilterProperties(System.Collections.IDictionary properties)
        {
            // 提供有关组件属性 (Attribute) 的信息，如组件的属性 (Attribute)、属性 (Property) 和事件。
            // 删除Address属性。
            properties.Remove("Address");
        }
        // PostFilterAttributes、PostFilterEvents、PreFilterAttributes、PreFilterEvents、PreFilterProperties请大家自己研究。

        /*
         对Component设置默认属性（过时，保持向下兼容），现在改为InitializeNewComponent中进行初始化。
        */
        public override void OnSetComponentDefaults()
        {
            base.OnSetComponentDefaults();
            //((Customer)(this.Component)).Age = 18;
        }
    }


    // DesignerActionList-derived class defines smart tag entries and
    // resultant actions.
    public class MenuTagActionList : DesignerActionList
    {
        private ItemCollectionEditor editorControl;
        private DesignerActionUIService designerActionUISvc = null;

        public MenuTagActionList(IComponent component)
            : base(component)
        {
            this.editorControl = component as ItemCollectionEditor;
            //用于刷新智能标记面板
            this.designerActionUISvc =  GetService(typeof(DesignerActionUIService)) as DesignerActionUIService;
        }

        // Helper method to retrieve control properties. Use of 
        // GetProperties enables undo and menu updates to work properly.
        private PropertyDescriptor GetPropertyByName(String propName)
        {
            PropertyDescriptor prop;
            prop = TypeDescriptor.GetProperties(editorControl)[propName];
            if (null == prop)
                throw new ArgumentException("Matching ColorLabel property not found!", propName);
            else
                return prop;
        }

        // Properties that are targets of DesignerActionPropertyItem entries.
        public Color BackColor
        {
            get
            {
                return editorControl.BackColor;
            }
            set
            {
                GetPropertyByName("BackColor").SetValue(editorControl, value);
            }
        }

        public Color ForeColor
        {
            get
            {
                return editorControl.ForeColor;
            }
            set
            {
                GetPropertyByName("ForeColor").SetValue(editorControl, value);
            }
        }


        // Boolean properties are automatically displayed with binary 
        // UI (such as a checkbox).
        public bool LockColors
        {
            get
            {
                return editorControl.ColorLocked;
            }
            set
            {
                GetPropertyByName("ColorLocked").SetValue(editorControl, value);
                // Refresh the list.
                this.designerActionUISvc.Refresh(this.Component);
            }
        }

        public String Text
        {
            get
            {
                return editorControl.Text;
            }
            set
            {
                GetPropertyByName("Text").SetValue(editorControl, value);
            }
        }
        public Sex Sex
        {
            get
            { return editorControl.Sex; }
            set
            {
                TypeDescriptor.GetProperties(typeof(ItemCollectionEditor))["Sex"]
                  .SetValue(editorControl, value);
            }
        }

        //取反色
        public void InvertColors()
        {
            Color currentBackColor = editorControl.BackColor;
            BackColor = Color.FromArgb(
                255 - currentBackColor.R,
                255 - currentBackColor.G,
                255 - currentBackColor.B);

            Color currentForeColor = editorControl.ForeColor;
            ForeColor = Color.FromArgb(
                255 - currentForeColor.R,
                255 - currentForeColor.G,
                255 - currentForeColor.B);
        }

        // 重写父类的方法，实现在智能标签显示的属性
        public override DesignerActionItemCollection GetSortedActionItems()
        {
            DesignerActionItemCollection items = new DesignerActionItemCollection();

            //Define static section header entries.
            items.Add(new DesignerActionHeaderItem("Appearance"));
            items.Add(new DesignerActionHeaderItem("Information"));

            //Boolean property for locking color selections.
            items.Add(new DesignerActionPropertyItem("LockColors",
                             "Lock Colors", "Appearance",
                             "Locks the color properties."));
            items.Add(new DesignerActionPropertyItem("Sex", "Sex", "Appearance", "Customer's sex"));
            if (!LockColors)
            {
                items.Add(new DesignerActionPropertyItem("BackColor",
                                 "Back Color", "Appearance",
                                 "Selects the background color."));
                items.Add(new DesignerActionPropertyItem("ForeColor",
                                 "Fore Color", "Appearance",
                                 "Selects the foreground color."));

                //This next method item is also added to the context menu 
                // (as a designer verb).
                items.Add(new DesignerActionMethodItem(this,
                                 "InvertColors", "Invert Colors",
                                 "Appearance",
                                 "Inverts the fore and background colors.",
                                  true));
            }
            items.Add(new DesignerActionPropertyItem("Text",
                             "Text String", "Appearance",
                             "Sets the display text."));

            //Create entries for static Information section.
            items.Add(new DesignerActionTextItem("location",
                             "Information"));

            return items;
        }
    }


}