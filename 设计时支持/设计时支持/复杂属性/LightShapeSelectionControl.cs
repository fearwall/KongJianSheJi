﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace 设计时支持
{
    [ToolboxItem(false)]
    public partial class LightShapeSelectionControl : UserControl
    {
        private MarqueeLightShape lightShapeValue = MarqueeLightShape.Square;

        private void squarePanel_Click(object sender, EventArgs e)
        {
            this.lightShapeValue = MarqueeLightShape.Square;

            this.Invalidate(false);

            this.editorService.CloseDropDown();
        }

        private void circlePanel_Click(object sender, EventArgs e)
        {
            this.lightShapeValue = MarqueeLightShape.Circle;

            this.Invalidate(false);

            this.editorService.CloseDropDown();
        }
        private IWindowsFormsEditorService editorService = null;
        // This constructor takes a MarqueeLightShape value from the
        // design-time environment, which will be used to display
        // the initial state.
        public LightShapeSelectionControl(
            MarqueeLightShape lightShape,
            IWindowsFormsEditorService editorService)
        {
            // This call is required by the designer.
            InitializeComponent();

            // Cache the light shape value provided by the 
            // design-time environment.
            this.lightShapeValue = lightShape;

            // Cache the reference to the editor service.
            this.editorService = editorService;

            // Handle the Click event for the two panels. 
            this.squarePanel.Click += new EventHandler(squarePanel_Click);
            this.squarePanel.MouseHover += squarePanel_MouseHover;
            this.squarePanel.MouseLeave += squarePanel_MouseLeave;
            this.circlePanel.Click += new EventHandler(circlePanel_Click);
        }

        void squarePanel_MouseLeave(object sender, EventArgs e)
        {
            squreEnter = false;
            this.Invalidate();
        }

        void squarePanel_MouseHover(object sender, EventArgs e)
        {
            squreEnter = true;
            this.Invalidate();
        }
        public MarqueeLightShape LightShape
        {
            get
            {
                return this.lightShapeValue;
            }

            set
            {
                if (this.lightShapeValue != value)
                {
                    this.lightShapeValue = value;
                }
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            using (
                Graphics gSquare = this.squarePanel.CreateGraphics(),
                gCircle = this.circlePanel.CreateGraphics())
            {
                // Draw a filled square in the client area of
                // the squarePanel control.
                gSquare.FillRectangle(
                    Brushes.Red,
                    0,
                    0,
                    this.squarePanel.Width,
                    this.squarePanel.Height
                    );

                // If the Square option has been selected, draw a 
                // border inside the squarePanel.
                if (this.lightShapeValue == MarqueeLightShape.Square)
                {
                    gSquare.DrawRectangle(
                        Pens.Black,
                        0,
                        0,
                        this.squarePanel.Width - 1,
                        this.squarePanel.Height - 1);
                }

                //if (squreEnter)
                //{
                //    gSquare.DrawRectangle(
                //       Pens.White,
                //       0,
                //       0,
                //       this.squarePanel.Width - 1,
                //       this.squarePanel.Height - 1);
                //}
                // Draw a filled circle in the client area of
                // the circlePanel control.
                gCircle.Clear(this.circlePanel.BackColor);
                gCircle.FillEllipse(
                    Brushes.Blue,
                    0,
                    0,
                    this.circlePanel.Width,
                    this.circlePanel.Height
                    );

                // If the Circle option has been selected, draw a 
                // border inside the circlePanel.
                if (this.lightShapeValue == MarqueeLightShape.Circle)
                {
                    gCircle.DrawRectangle(
                        Pens.Black,
                        0,
                        0,
                        this.circlePanel.Width - 1,
                        this.circlePanel.Height - 1);
                }
            }
        }

        private bool squreEnter;
        private bool circleEnter;

        private void squarePanel_Enter(object sender, EventArgs e)
        {
            squreEnter = true;
        }

        private void squarePanel_Leave(object sender, EventArgs e)
        {
            squreEnter = false;
        }
    }
}
