﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace 设计时支持
{
    public class GradeEditor : UITypeEditor
    {
        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand)]
        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            // UITypeEditorEditStyle有三种，Modal是弹出式，DropDown是下拉式，None是没有。
            return UITypeEditorEditStyle.Modal;
        }

        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand)]
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, System.IServiceProvider provider, object value)
        {
            // 得到editor service，可由其创建弹出窗口。
            IWindowsFormsEditorService editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            // context.Instance —— 可以得到当前的Demo3对象。
            // ((Demo3)context.Instance).Grade —— 可以得到当前Grade的值。
            string grade = ((ItemCollectionEditor)context.Instance).Grade;
            FrmGradeEditor dialog = new FrmGradeEditor();
            editorService.ShowDialog(dialog);
            if (!string.IsNullOrEmpty(dialog.Grade))
                grade = dialog.Grade;
            dialog.Dispose();
            return grade;
        }
    }
}
