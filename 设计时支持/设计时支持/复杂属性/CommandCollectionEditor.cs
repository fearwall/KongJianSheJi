﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace 设计时支持
{
    /// <summary>
    /// 集合属性编辑器
    /// </summary>
    public partial class CommandCollectionEditor : CollectionEditor
    {
        public CommandCollectionEditor(Type type)
            : base(type)
        { }
        protected override bool CanSelectMultipleInstances()
        {
            return true;
        }
        protected override Type[] CreateNewItemTypes()
        {
            return new Type[] { typeof(CommandItem), typeof(CommandSeperator) };
        }
        protected override object CreateInstance(Type itemType)
        {
            IDesignerHost host = (IDesignerHost)this.GetService(typeof(IDesignerHost));
            ItemBase item = null;
            if (itemType == typeof(CommandItem))
                item = new CommandItem();
            if (itemType == typeof(CommandSeperator))
                item = new CommandSeperator();
            //host.Container.Add(item);//重要！自动生成组件的设计时代码！ 
            return item;
        }

        private object GetCtrl(CollectionForm form, string name)
        {
            FieldInfo fieldInfo = form.GetType().GetField(name, BindingFlags.Instance | BindingFlags.NonPublic);
            if (fieldInfo != null)
                return fieldInfo.GetValue(form);
            return null;
        }

        protected override CollectionForm CreateCollectionForm()
        {
            CollectionForm form = base.CreateCollectionForm();
            PropertyGrid grid = GetCtrl(form, "propertyBrowser") as PropertyGrid;
            grid.HelpVisible = true;

            return form;
        }

        /// <summary>
        /// 修改的时候触发
        /// </summary>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            return base.EditValue(context, provider, value);
        }

        protected override string GetDisplayText(object value)
        {
            return value is CommandItem ? "菜单项" : "分割线";
        }

        protected override Type CreateCollectionItemType()
        {

            return base.CreateCollectionItemType();
        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return base.GetEditStyle(context);
        }

        /// <summary>
        /// 初始化得到已有项
        /// </summary>
        protected override object[] GetItems(object editValue)
        {
            return base.GetItems(editValue);
        }

        protected override System.Collections.IList GetObjectsFromInstance(object instance)
        {
            return base.GetObjectsFromInstance(instance);
        }

        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return base.GetPaintValueSupported(context);
        }

        protected override void ShowHelp()
        {
            base.ShowHelp();
        }

        protected override object SetItems(object editValue, object[] value)
        {
            return base.SetItems(editValue, value);
        }
    }
}
