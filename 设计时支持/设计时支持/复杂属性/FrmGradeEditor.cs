using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace 设计时支持
{
    public partial class FrmGradeEditor : Form
    {
        public string Grade;

        public FrmGradeEditor()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Grade = listGrade.SelectedItem.ToString();
            this.Close();
        }
    }
}