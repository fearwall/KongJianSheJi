﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Text;
using System.Windows.Forms.Design;

namespace 设计时支持
{
    // This defines the possible values for the MarqueeBorder
    // control's LightShape property.
    public enum MarqueeLightShape
    {
        Square,
        Circle
    }
    /// 命令项基类
    //[Serializable]
    public abstract class ItemBase
    {
        private MarqueeLightShape lightShapeValue = MarqueeLightShape.Square;
        [Category("Marquee")]
        [Browsable(true)]
        [Description("形状选择")]
        [EditorAttribute(typeof(LightShapeEditor),
             typeof(System.Drawing.Design.UITypeEditor))]
        public MarqueeLightShape LightShape
        {
            get
            {
                return this.lightShapeValue;
            }

            set
            {
                this.lightShapeValue = value;
            }
        }
        private bool _EnableViewState = true;
        [Description("可用状态")]
        public bool EnableViewState
        {
            get
            {
                return _EnableViewState;
            }
            set
            {
                _EnableViewState = value;
            }
        }
    }
    internal class LightShapeEditor : UITypeEditor
    {
        private IWindowsFormsEditorService editorService = null;
        public override UITypeEditorEditStyle GetEditStyle(
            System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }
        public override object EditValue(
ITypeDescriptorContext context,
IServiceProvider provider,
object value)
        {
            if (provider != null)
            {
                editorService =
                    provider.GetService(
                    typeof(IWindowsFormsEditorService))
                    as IWindowsFormsEditorService;
            }

            if (editorService != null)
            {
                LightShapeSelectionControl selectionControl = new LightShapeSelectionControl((MarqueeLightShape)value, editorService);

                editorService.DropDownControl(selectionControl);

                value = selectionControl.LightShape;
            }

            return value;
        }

    }
    /// <summary>
    /// 命令项枚举
    /// </summary>
    public enum CommandActionType
    {
        //保存
        Save,
        //新增
        Add,
        //编辑
        Edit,
        //删除
        Delete,
        //关闭
        Close
        //...
    }
    ///  命令按钮类
    ///
    [ToolboxItem(false)]
    //[Serializable]
    public class CommandItem : ItemBase
    {
        private CommandCollection items;
        [Description("工具按钮集设置")]
        [Category("集合设置")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public CommandCollection Items
        {
            get
            {
                if (items == null)
                {
                    items = new CommandCollection();
                }
                return items;
            }
        }
        
        private CommandActionType _CommandActionType;
        public CommandItem()
        {
        }
        public CommandItem(CommandActionType commandActionType, string strText, string strAccessKey, string strToolTip)
        {
            this._CommandActionType = commandActionType;
        }
        /// 命令按钮类型
        [NotifyParentProperty(true)]
        [Description("命令按钮类型")]
        public CommandActionType CommandActionType
        {
            get
            {
                return _CommandActionType;
            }
            set
            {
                _CommandActionType = value;
            }
        }
    }

    /// <summary>
    /// 分隔符类
    /// </summary>
    [ToolboxItem(false)]
    //[Serializable]
    public class CommandSeperator : ItemBase
    {
        private uint width;
        private uint Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        private uint height;
        private uint Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }
    }

    /// <summary>
    /// 工具按钮集合类
    /// </summary>
    [ToolboxItem(false)]
    //[ParseChildren(true)]
    //[Serializable]
    [Editor(typeof(CommandCollectionEditor), typeof(UITypeEditor))]
    public class CommandCollection : CollectionBase //Collection<ItemBase>
    {
        #region 定义构造函数

        public CommandCollection() : base()
        {
        }

        #endregion

        /// <summary>
        /// 得到集合元素的个数
        /// </summary>
        public new int Count
        {
            get
            {
                return base.Count;
            }
        }

        public void Add(ItemBase item)
        {
            List.Add(item);
        }
    }
}
