﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmartTags;
using System.Drawing.Design;

namespace 设计时支持
{
    [DefaultProperty("ToolBarItems")]
    [Designer(typeof(MenuTagDesigner))]
    public partial class ItemCollectionEditor : Component
    {
        public bool ColorLocked { get; set; }
        public Color BackColor { get; set; }
        public Color ForeColor { get; set; }
        public string Text { get; set; }
        private Sex _sex;
        public Sex Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }
        private CommandCollection toolBarItems = new CommandCollection();
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Description("工具按钮集设置")]
        [Category("集合设置")]
        public CommandCollection ToolBarItems
        {
            get
            {
                if (toolBarItems == null)
                {
                    toolBarItems = new CommandCollection();
                }
                return toolBarItems;
            }
        }
        private string grade = "一年级";
        // 用户自定义Designer。
        [Editor(typeof(GradeEditor), typeof(UITypeEditor)), LocalizableAttribute(true)]
        public string Grade
        {
            get { return grade; }
            set { grade = value; }
        }
    }
    public enum Sex
    {
        男, 女
    }
}
