﻿namespace Ax.Windows.Forms
{
    partial class MenuToolBar
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipEx = new ToolTipsEx(this.components);
            this.SuspendLayout();
            // 
            // toolTipEx1
            // 
            this.toolTipEx.AutoPopDelay = 5000;
            this.toolTipEx.InitialDelay = 500;
            this.toolTipEx.OwnerDraw = true;
            this.toolTipEx.ReshowDelay = 800;
            // 
            // ToolBtn
            // 
            this.Name = "ToolControl";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private ToolTipsEx toolTipEx;
    }
}
