﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Utilities;

namespace Utilities
{
    /// <summary>
    /// 集合属性编辑器
    /// </summary>
    public partial class ItemCollectionEditor : CollectionEditor
    {
        public ItemCollectionEditor(Type type)
            : base(type)
        { }
        protected override bool CanSelectMultipleInstances()
        {
            return false;
        }

        protected override Type[] CreateNewItemTypes()
        {
            return new Type[] { typeof(Utilities.TextItem), typeof(SplitItem) };
        }

        protected override object CreateInstance(Type itemType)
        {
            IDesignerHost host = (IDesignerHost)this.GetService(typeof(IDesignerHost));
           
            if (itemType == typeof(Utilities.TextItem))
            {
                Utilities.TextItem menuitem = new Utilities.TextItem();
                host.Container.Add(menuitem);
                menuitem.Text = menuitem.Site.Name;
                return menuitem;
            }
            else {
                SplitItem splitItem = new SplitItem();
                host.Container.Add(splitItem);
                return splitItem;
            } 
        }

        private object GetCtrl(CollectionForm form, string name)
        {
            FieldInfo fieldInfo = form.GetType().GetField(name, BindingFlags.Instance | BindingFlags.NonPublic);
            if (fieldInfo != null)
                return fieldInfo.GetValue(form);
            return null;
        }

        protected override CollectionForm CreateCollectionForm()
        {
            CollectionForm form = base.CreateCollectionForm();
            form.Text = "菜单项集合编辑器";
            PropertyGrid grid = GetCtrl(form, "propertyBrowser") as PropertyGrid;
            grid.HelpVisible = true;
            return form;
        }

        /// <summary>
        /// 修改的时候触发
        /// </summary>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            return base.EditValue(context, provider, value);
        }

        protected override string GetDisplayText(object value)
        {
            ((BaseMenuItem)value).Name = ((Component)value).Site.Name;
          
            return ((Component)value).Site.Name;
        }

        protected override Type CreateCollectionItemType()
        { 
            return base.CreateCollectionItemType();
        }

        public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return base.GetEditStyle(context);
        }

        /// <summary>
        /// 初始化得到已有项
        /// </summary>
        protected override object[] GetItems(object editValue)
        {
            return base.GetItems(editValue);
        }

        protected override System.Collections.IList GetObjectsFromInstance(object instance)
        {
            return base.GetObjectsFromInstance(instance);
        }

        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return base.GetPaintValueSupported(context);
        }

        protected override void ShowHelp()
        {
            base.ShowHelp();
        }

        protected override object SetItems(object editValue, object[] value)
        {
            return base.SetItems(editValue, value);
        }
    }
}
