﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Utilities;

namespace Ax.Windows.Forms
{
    /// <summary>
    /// 菜单工具条按钮
    /// </summary>
    [ToolboxItem(false)]
    public class MenuBtn : BaseBtn
    {
        [Description("只显示图片"), DefaultValue(false)]
        public bool ImageOnly { get; set; }
        [Description("是否响应鼠标事件"), DefaultValue(true)]
        public bool EventResponse { get; set; }
        /// <summary>
        /// 是否显示分割线
        /// </summary>
        [Description("显示分割线"), DefaultValue(false)]
        public bool ShowSplit { get; set; }
        /// <summary>
        /// 父按钮
        /// </summary>
        [Browsable(false)]
        public MenuBtn Owner { get; set; }
        /// <summary>
        /// 是否自动回弹
        /// </summary>
        [Description("按钮是否自动弹回"), DefaultValue(true)]
        public bool AutoRebound { get; set; }

        internal MenuToolBar ToolBar;
        Utilities.ContextMenuStrip menuStrip;

        [Description("子按钮集合"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public MenuToolBtnCollection Btns { get; set; }
        public event Action<MenuBtn> BtnClicked;

        /// <summary>
        /// 弹出菜单项集合
        /// </summary>
        [Browsable(false)]
        public MenuItemCollection MenuItems
        {
            get { return menuStrip.Items; }
        }

        public MenuBtn()
        {
            AutoRebound = true;
            EventResponse = true;
            Btns = new MenuToolBtnCollection();
            menuStrip = new Utilities.ContextMenuStrip();
        }

        public MenuBtn(MenuToolBar ctrl)
        {
            AutoRebound = true;
            EventResponse = true;
            ToolBar = ctrl;
            Btns = new MenuToolBtnCollection();
            menuStrip = new Utilities.ContextMenuStrip();
        }

        public void AddChild(MenuBtn btn)
        {
            Btns.Add(btn);
            btn.Owner = this;
            //添加完按钮后继续添加菜单项
            var item = new TextItem(btn.Text);
            item.Image = btn.Image;
            item.Checked = btn.Checked;
            item.Enabled = btn.Enabled;
            item.Name = btn.Name;
            item.Tag = btn;
            item.Click += menu_Click;
            menuStrip.Items.Add(item);
            if (btn.ShowSplit)
                menuStrip.Items.Add(new SplitItem());
        }

        public void ShowMenu(Control control, Point point)
        {
            menuStrip.Show(control, point);
        }

        void menu_Click(TextItem item)
        {
            foreach (MenuBtn menuBtn in ToolBar)
            {
                menuBtn.Checked = false;
            }
            MenuBtn btn = item.Tag as MenuBtn;
            foreach (MenuBtn menuBtn in btn.Owner.Btns)
            {
                menuBtn.Checked = false;
            }
            btn.Checked = true;
            OnBtnClicked(btn);
            menuStrip.Hide();
        }

        private void OnBtnClicked(MenuBtn btn)
        {
            if (BtnClicked != null)
            {
                BtnClicked(btn);
            }
        }
    }

    /// <summary>
    /// 按钮基类
    /// </summary>
    [ToolboxItem(false)]
    [DesignTimeVisible(false)]
    public class BaseBtn : Component
    {
        [Description("与对象关联的文本")]
        public string Text { get; set; }
        [Description("对象名称")]
        public string Name { get; set; }
        [Description("将在控件上显示的图像")]
        public Image Image { get; set; }
        [Description("按钮提示语")]
        public string Tips { get; set; }
        [Description("与对象关联的数据"), TypeConverter(typeof(StringConverter))]
        public object Tag { get; set; }
        [Description("控件可用性"), DefaultValue(true)]
        public bool Enabled { get; set; }
        /// <summary>
        /// 是否选中
        /// </summary>
        [Description("按钮是否勾选"), DefaultValue(false)]
        public bool Checked { get; set; }
        /// <summary>
        /// 图片靠左
        /// </summary>
        [Description("图片靠左"), DefaultValue(true)]
        public bool ImageFirst { get; set; }
        [Description("按钮宽度")]
        public int Width { get; set; }
        internal Rectangle Bounds { get; set; }
        /// <summary>
        /// 是否按下
        /// </summary>
        internal bool Pressed;

        public BaseBtn()
        {
            Enabled = true;
            ImageFirst = true;
            Width = 68;
        }
    }
}
