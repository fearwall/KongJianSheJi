﻿namespace ToolMenuBar
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuToolBar1 = new Ax.Windows.Forms.MenuToolBar();
            this.menuBtn1 = new Ax.Windows.Forms.MenuBtn();
            this.menuBtn2 = new Ax.Windows.Forms.MenuBtn();
            this.menuBtn3 = new Ax.Windows.Forms.MenuBtn();
            this.SuspendLayout();
            // 
            // menuToolBar1
            // 
            this.menuToolBar1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuToolBar1.BoderColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.menuToolBar1.Btns.Add(this.menuBtn1);
            this.menuToolBar1.Btns.Add(this.menuBtn2);
            this.menuToolBar1.Btns.Add(this.menuBtn3);
            this.menuToolBar1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.menuToolBar1.ForeColor = System.Drawing.Color.Black;
            this.menuToolBar1.Location = new System.Drawing.Point(37, 37);
            this.menuToolBar1.MaxWidth = 0;
            this.menuToolBar1.MoreBtnImage = ((System.Drawing.Image)(resources.GetObject("menuToolBar1.MoreBtnImage")));
            this.menuToolBar1.Name = "menuToolBar1";
            this.menuToolBar1.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.menuToolBar1.SelectColor = System.Drawing.Color.Black;
            this.menuToolBar1.Size = new System.Drawing.Size(136, 23);
            this.menuToolBar1.SplitLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.menuToolBar1.TabIndex = 0;
            this.menuToolBar1.Text = "menuToolBar1";
            // 
            // menuBtn1
            // 
            this.menuBtn1.Image = null;
            this.menuBtn1.Name = "menuBtn1";
            this.menuBtn1.Owner = null;
            this.menuBtn1.Tag = null;
            this.menuBtn1.Text = "menuBtn1";
            this.menuBtn1.Tips = null;
            this.menuBtn1.Width = 68;
            // 
            // menuBtn2
            // 
            this.menuBtn2.Image = null;
            this.menuBtn2.Name = "menuBtn2";
            this.menuBtn2.Tag = null;
            this.menuBtn2.Text = "menuBtn2";
            this.menuBtn2.Tips = null;
            this.menuBtn2.Width = 68;
            // 
            // menuBtn3
            // 
            this.menuBtn3.Image = null;
            this.menuBtn3.Name = "menuBtn3";
            this.menuBtn3.Tag = null;
            this.menuBtn3.Text = "menuBtn3";
            this.menuBtn3.Tips = null;
            this.menuBtn3.Width = 68;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.menuToolBar1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private Ax.Windows.Forms.MenuToolBar menuToolBar1;
        private Ax.Windows.Forms.MenuBtn menuBtn1;
        private Ax.Windows.Forms.MenuBtn menuBtn2;
        private Ax.Windows.Forms.MenuBtn menuBtn3;
    }
}

