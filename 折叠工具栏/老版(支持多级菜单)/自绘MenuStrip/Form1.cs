﻿using System;
using System.Windows.Forms;
using ToolBtn;

namespace 自绘MenuStrip
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            MessageBox.Show("1");
            InitToolBtn();
            MessageBox.Show("2");
        }

        void InitToolBtn()
        {
            BtnPopup btnPop = new BtnPopup(toolBtn) { Text = "菜单", Name = "菜单", Image = imageList.Images[1] };
            toolBtn.Add(btnPop);
            BtnBase btnBase = new BtnBase(toolBtn) { Text = "子菜单", Name = "子菜单", Tips = "子菜单" };
            btnPop.AddChild(btnBase);
            BtnPopup btnChildPop = new BtnPopup(toolBtn) { Text = "菜单", Name = "菜单", Tips = "菜单" };
            BtnPopup btnChildBase = new BtnPopup(toolBtn) { Text = "二级菜单", Name = "二级菜单", Tips = "二级菜单" };
            BtnBase btn3 = new BtnBase(toolBtn) { Text = "三级菜单", Name = "三级菜单"};
            btnChildBase.AddChild(btn3);
            btnChildPop.AddChild(btnChildBase);
            btnPop.AddChild(btnChildPop);
            for (int i = 1; i < 8;i++)
            {
                BtnBase btn = toolBtn.Add("按钮" + i.ToString());
                btn.Tips = "按钮" + i.ToString();
                btn.Image = imageList.Images[0];
            }
            toolBtn.OnBtnClicked += toolBtn_OnBtnClicked;
            toolBtn.MoreImage = imageList.Images[1];
            toolBtn.Width = 100;
        }

        void toolBtn_OnBtnClicked(object obj, string name)
        {
            textBox1.Text = name;
        }

        private void textBox1_MouseEnter(object sender, EventArgs e)
        {
            textBox1.Focus();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.Focus();
        }
    }
}
