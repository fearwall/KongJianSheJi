﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Forms
{
    public partial class MainForm : MyBaseForm
    {
        public MainForm()
        {
            InitializeComponent();
            BtnBoxs.AddBtn("主题", "Theme");
            this.OnBtnClicked += MainForm_OnBtnClicked;
        }

        void MainForm_OnBtnClicked(string name)
        {
            MessageBox.Show(name);
        }

    }
}
