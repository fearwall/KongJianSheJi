﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Forms
{
    public class BtnBoxs
    {
        #region 私有函数
        List<BtnBox> BtnBoxList;
        /// <summary>
        /// 字体
        /// </summary>
        public Font Font;
        /// <summary>
        /// 鼠标经过列
        /// </summary>
        int HoverIndex = -1;
        /// <summary>
        /// 鼠标按下所在列
        /// </summary>
        int PressIndex = -1;
        /// <summary>
        /// 主窗体宽度
        /// </summary>
        int FormWidth;
        #endregion

        #region 公有函数
        public delegate void OnBtnClickHandle(string name);
        public event OnBtnClickHandle OnBtnClicked;
        /// <summary>
        /// 按钮个数
        /// </summary>
        public int Count
        {
            get { return BtnBoxList.Count; }
        }
        /// <summary>
        /// 按钮大小
        /// </summary>
        public Size BtnSize = new Size(36, 21);
        #endregion

        public BtnBoxs()
        {
            BtnBoxList = new List<BtnBox>();
        }

        /// <summary>
        /// 添加按钮
        /// </summary>
        /// <param name="txt">文本</param>
        /// <param name="name">名字</param>
        /// <param name="image">图片</param>
        public void AddBtn(string txt, string name = null, Image image = null)
        {
            BtnBoxList.Add(new BtnBox(){ Text = txt, Name = name, Image = image, BtnSize = this.BtnSize});
        }

        public void AddBtn(BtnBox btn)
        {
            BtnBoxList.Add(btn);
        }

        /// <summary>
        /// 主绘制函数
        /// </summary>
        /// <param name="g"></param>
        public void OnPaint(Graphics g)
        {
            for (int i = 0; i < BtnBoxList.Count; i++)
            {
                BtnBox btn = BtnBoxList[i];
                Point LeftTopPt = new Point(FormWidth - BtnSize.Width * (i + 1), 0);
                if (PressIndex == i)
                    btn.DrawPress(g, LeftTopPt);
                if (HoverIndex == i)
                    btn.DrawHover(g, LeftTopPt);
                DrawBtn(g, btn, LeftTopPt);
            }
        }
        
        #region 鼠标事件处理函数
        public void OnMouseMove(Point pt)
        {
            if (IsInside(pt))
                HoverIndex = (FormWidth - pt.X) / BtnSize.Width;
            else
            {
                HoverIndex = -1;
                PressIndex = -1;
            }
        }

        public void OnSizeChanged(int width)
        {
            FormWidth = width;
        }

        public void OnMouseLBtnDown(Point pt)
        {
            PressIndex = IsInside(pt) ? (FormWidth - pt.X) / BtnSize.Width : -1;
        }

        public void OnMouseLBtnUp(Point pt)
        {
            if (IsInside(pt))
            {
                int index = (FormWidth - pt.X) / BtnSize.Width;
                if (OnBtnClicked != null)
                {
                    OnBtnClicked(BtnBoxList[index].Name);
                }

            }
            PressIndex = -1;
        }
        #endregion

        bool IsInside(Point pt)
        {
            if (pt.X >= FormWidth - BtnBoxList.Count * BtnSize.Width && pt.Y <= BtnSize.Height)
                return true;
            return false;
        }

        /// <summary>
        /// 绘制按钮文字或图片
        /// </summary>
        /// <param name="g"></param>
        /// <param name="btn">按钮实例</param>
        /// <param name="position">按钮左上角左边</param>
        protected void DrawBtn(Graphics g,BtnBox btn, Point position)
        {
            if (btn.Image == null)
            {
                Size TextSize = TextRenderer.MeasureText(btn.Text, Font);
                g.DrawString(btn.Text, Font, new SolidBrush(Color.Black), new Point(position.X + (BtnSize.Width - TextSize.Width) / 2, position.Y + (BtnSize.Height - TextSize.Height) / 2));
            }
            else
                g.DrawImage(btn.Image, new Point(position.X + (BtnSize.Width - btn.Image.Width) / 2, position.Y + (BtnSize.Height - btn.Image.Height) / 2));
        }
    }

    public class CloseBtnBox : BtnBox
    {
        public override void DrawHover(Graphics g, Point position)
        {
            g.FillRectangle(new SolidBrush(Color.FromArgb(231, 87, 87)), new Rectangle(position, BtnSize));
        }
        public override void DrawPress(Graphics g, Point position)
        {
            g.FillRectangle(new SolidBrush(Color.FromArgb(225, 61, 62)), new Rectangle(position, BtnSize));
        }
    }

    public class BtnBox
    {
        public Size BtnSize;
        public virtual void DrawHover(Graphics g, Point position)
        {
            g.DrawRectangle(new Pen(Color.FromArgb(202, 202, 202)), new Rectangle(position, BtnSize));
        }
        public virtual void DrawPress(Graphics g, Point position)
        {
            g.FillRectangle(new SolidBrush(Color.FromArgb(225, 225, 225)), new Rectangle(position, BtnSize));
        }
        public string Text { get; set; }
        public string Name { get; set; }
        public Image Image { get; set; }
    }
}
