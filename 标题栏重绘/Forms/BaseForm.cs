﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Forms
{
    public partial class MyBaseForm : Form
    {
        public int TitleHeight = 54;
        bool IsLBtnDown = false;
        Point OldPoint;
        public BtnBoxs BtnBoxs = new BtnBoxs();
        const int HTLEFT = 10;
        const int HTRIGHT = 11;
        const int HTTOP = 12;
        const int HTTOPLEFT = 13;
        const int HTTOPRIGHT = 14;
        const int HTBOTTOM = 15;
        const int HTBOTTOMLEFT = 0x10;
        const int HTBOTTOMRIGHT = 17;

        public delegate void OnBtnClickHandle(string name);
        public event OnBtnClickHandle OnBtnClicked;

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            switch (m.Msg)
            {
                case 0x0084:
                    if (WindowState != FormWindowState.Maximized)
                    {
                        Point vPoint = new Point((int)m.LParam & 0xFFFF,
                            (int)m.LParam >> 16 & 0xFFFF);
                        vPoint = PointToClient(vPoint);
                        if (vPoint.X <= 5)
                            if (vPoint.Y <= 5)
                                m.Result = (IntPtr)HTTOPLEFT;
                            else if (vPoint.Y >= ClientSize.Height - 5)
                                m.Result = (IntPtr)HTBOTTOMLEFT;
                            else m.Result = (IntPtr)HTLEFT;
                        else if (vPoint.X >= ClientSize.Width - 5)
                            if (vPoint.Y <= 5)
                                m.Result = (IntPtr)HTTOPRIGHT;
                            else if (vPoint.Y >= ClientSize.Height - 5)
                                m.Result = (IntPtr)HTBOTTOMRIGHT;
                            else m.Result = (IntPtr)HTRIGHT;
                        else if (vPoint.Y <= 5)
                            m.Result = (IntPtr)HTTOP;
                        else if (vPoint.Y >= ClientSize.Height - 5)
                            m.Result = (IntPtr)HTBOTTOM;
                    }
                    break;
                case 0x0201://鼠标左键按下的消息 
                    //m.Msg = 0x00A1;//更改消息为非客户区按下鼠标 
                    //m.LParam = IntPtr.Zero;//默认值 
                    //m.WParam = new IntPtr(2);//鼠标放在标题栏内 
                    break;
                default:
                    break;
            }
        }
        public MyBaseForm()
        {
            InitializeComponent();
            BtnBoxs.AddBtn(new CloseBtnBox() { BtnSize = BtnBoxs.BtnSize, Text = "关闭", Name = "Close", Image = imageList.Images[2] });
            BtnBoxs.AddBtn("最大化", "Max", imageList.Images[1]);
            BtnBoxs.AddBtn("最小化", "Min", imageList.Images[0]);
            BtnBoxs.Font = Font;
            BtnBoxs.OnBtnClicked += BtnBoxs_OnBtnClicked;
            this.Padding = new System.Windows.Forms.Padding(1, TitleHeight, 1, 1);
            this.MaximumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height); 
            this.FormBorderStyle = FormBorderStyle.None;
            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
        }

        void BtnBoxs_OnBtnClicked(string name)
        {
            switch (name)
            {
                case "Close":
                    this.Close();
                    break;
                case "Min":
                    WindowState = FormWindowState.Minimized;
                    IsLBtnDown = false;
                    break;
                case "Max":
                    WindowState = WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
                    break;
                default:
                    if (OnBtnClicked != null)
                        OnBtnClicked(name);
                    break;
            }
        }

        bool IsInTitleRect(Point point)
        {
            return point.Y <= TitleHeight;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(241,241,241)), new Rectangle(0, 0, Width, TitleHeight));
            BtnBoxs.OnPaint(e.Graphics);
            e.Graphics.DrawRectangle(new Pen(Color.FromArgb(121, 121, 121)), new Rectangle(0, 0, Width - 1, Height - 1));
        }

        protected override CreateParams CreateParams
        {
            get
            {
                const int WS_MINIMIZEBOX = 0x00020000;
                CreateParams cp = base.CreateParams;
                cp.Style = cp.Style | WS_MINIMIZEBOX;   // 允许最小化操作
                return cp;
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                if (IsInTitleRect(e.Location))
                {
                    if (!IsClickBtns(e.Location))
                    {
                        IsLBtnDown = true;
                        OldPoint = e.Location;
                    }
                }
                BtnBoxs.OnMouseLBtnDown(e.Location);
                Invalidate();
            }
        }

        bool IsClickBtns(Point point)
        {
            if (point.X >= Width - BtnBoxs.Count * BtnBoxs.BtnSize.Width)
            {
                if (point.Y <= BtnBoxs.BtnSize.Height)
                {
                    return true;
                }
            }
            return false;
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (e.Y <= TitleHeight)
            {
                WindowState = WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left)
            {
                IsLBtnDown = false;
                BtnBoxs.OnMouseLBtnUp(e.Location);
                Invalidate();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (IsLBtnDown)
            {
                this.Location = new Point(Location.X + e.Location.X - OldPoint.X, Location.Y + e.Location.Y - OldPoint.Y);
            }
            BtnBoxs.OnMouseMove(e.Location);
            Invalidate();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            BtnBoxs.OnSizeChanged(Width);
            Invalidate();
        }
    }
}
