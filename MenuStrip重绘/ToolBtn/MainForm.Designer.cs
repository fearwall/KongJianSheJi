﻿namespace ToolBtn
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.测试项1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.测试项2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.测试项3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.子菜单1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.子菜单2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.测试项4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.测试项5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.测试项1ToolStripMenuItem,
            this.测试项2ToolStripMenuItem,
            this.测试项3ToolStripMenuItem,
            this.toolStripSeparator1,
            this.测试项4ToolStripMenuItem,
            this.测试项5ToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(120, 120);
            // 
            // 测试项1ToolStripMenuItem
            // 
            this.测试项1ToolStripMenuItem.Name = "测试项1ToolStripMenuItem";
            this.测试项1ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.测试项1ToolStripMenuItem.Text = "测试项1";
            // 
            // 测试项2ToolStripMenuItem
            // 
            this.测试项2ToolStripMenuItem.Name = "测试项2ToolStripMenuItem";
            this.测试项2ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.测试项2ToolStripMenuItem.Text = "测试项2";
            // 
            // 测试项3ToolStripMenuItem
            // 
            this.测试项3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.子菜单1ToolStripMenuItem,
            this.子菜单2ToolStripMenuItem});
            this.测试项3ToolStripMenuItem.Name = "测试项3ToolStripMenuItem";
            this.测试项3ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.测试项3ToolStripMenuItem.Text = "测试项3";
            // 
            // 子菜单1ToolStripMenuItem
            // 
            this.子菜单1ToolStripMenuItem.Name = "子菜单1ToolStripMenuItem";
            this.子菜单1ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.子菜单1ToolStripMenuItem.Text = "子菜单1";
            // 
            // 子菜单2ToolStripMenuItem
            // 
            this.子菜单2ToolStripMenuItem.Name = "子菜单2ToolStripMenuItem";
            this.子菜单2ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.子菜单2ToolStripMenuItem.Text = "子菜单2";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(116, 6);
            // 
            // 测试项4ToolStripMenuItem
            // 
            this.测试项4ToolStripMenuItem.Name = "测试项4ToolStripMenuItem";
            this.测试项4ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.测试项4ToolStripMenuItem.Text = "测试项4";
            // 
            // 测试项5ToolStripMenuItem
            // 
            this.测试项5ToolStripMenuItem.Name = "测试项5ToolStripMenuItem";
            this.测试项5ToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.测试项5ToolStripMenuItem.Text = "测试项5";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ContextMenuStrip = this.contextMenuStrip;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem 测试项1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 测试项2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 测试项3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 测试项4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 子菜单1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 子菜单2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 测试项5ToolStripMenuItem;
    }
}

